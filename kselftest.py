import jobs.report
import jobs.squad
import os
from requests.compat import urljoin
from squad_client.core.api import SquadApi

# TODO: arg
SquadApi.configure(url=os.environ["SQUAD_URL"])

# TODO: arg
# import requests_cache
# requests_cache.install_cache("kselftest")

# TODO: validate
group, project, build, build2 = jobs.squad.parse_build_string(
    f"lkft,linux-next-master,{os.environ['SQUAD_BUILD']}"
)

build_details_url = urljoin(
    SquadApi.url, "%s/%s/build/%s" % (group.slug, project.slug, build.version)
)

environments = jobs.squad.get_environments(project)
suites = jobs.squad.get_suites(project, ["kselftest"])

# TODO: build_data.py
summary, fails, skips = jobs.squad.get_summary(build, environments, suites)

regressions, fixes = jobs.squad.parse_changes(
    jobs.squad.get_changes(project, build2.id, build.id), environments, suites
)

te = jobs.report.build_template_env("templates/kselftest")

# TODO: url
# TODO: subject
# TODO: prev build
body = te.get_template("body.txt.jinja").render(
    build=build,
    build2=build2,
    build_details_url=build_details_url,
    environments=environments,
    suites=suites,
    summary=summary,
    regressions=regressions,
    fixes=fixes,
    fails=fails,
    skips=skips,
)

with open("body.txt", "w") as f:
    f.write(body)
