import jobs.squad
import json
import logging
import os

# TODO: arg
# import requests_cache
# requests_cache.install_cache("build_data")

try:

    build_key = jobs.squad.BuildKey(
        os.environ["SQUAD_URL"],
        os.environ["SQUAD_GROUP"],
        os.environ["SQUAD_PROJECT"],
        os.environ["SQUAD_BUILD"],
    )

    results = jobs.squad.build_results(build_key)

    with open("results.json", "w") as f:
        json.dump(results, f)

except KeyError as ke:
    logging.exception(ke)

except jobs.squad.BuildNotFinished as bnf:
    logging.exception(bnf)
