import collections
import copy
from squad_client.core.api import SquadApi
from squad_client.core.models import Squad, Environment, Suite
from squad_client.utils import getid, first


# TODO: order="slug"
def get_environments(project):
    unsorted_environments = list(project.environments().values())

    return sorted(unsorted_environments, key=lambda Environment: Environment.slug)


# TODO: order="slug"
def get_suites(project, slugs=[]):
    if len(slugs):
        unsorted_suites = list(project.suites(slug__in=",".join(slugs)).values())
    else:
        unsorted_suites = list(project.suites().values())

    return sorted(unsorted_suites, key=lambda Suite: Suite.slug)


# TODO: order="slug"
def get_tests(build, environment, suite):
    # TODO: https://github.com/Linaro/squad-client/issues/104
    b = copy.deepcopy(build)
    unsorted_tests = list(b.tests(environment=environment.id, suite=suite.id).values())

    return sorted(unsorted_tests, key=lambda Test: Test.short_name)


def get_changes(project, base, prev):
    return project.compare_builds(base, prev)


# TODO: build template data, return dict
# TODO: inject tests as arg
# TODO: move to better location
def get_summary(build, environments, suites):
    summary = {}
    fails = {}
    skips = {}

    for e in environments:
        for s in suites:
            key = (e.slug, s.slug)
            summary[key] = {"pass": 0, "fail": 0, "skip": 0, "xfail": 0}
            fails[key] = []
            skips[key] = []

            for t in get_tests(build, e, s):
                # TODO: else error
                if t.status == "pass":
                    summary[key]["pass"] += 1
                elif t.status == "fail":
                    summary[key]["fail"] += 1
                    fails[key].append(t)
                elif t.status == "skip":
                    summary[key]["skip"] += 1
                    skips[key].append(t)
                elif t.status == "xfail":
                    summary[key]["xfail"] += 1

    return summary, fails, skips


def parse_build_string(build_string):
    names = build_string.split(",", maxsplit=3)

    group = Squad().group(names[0])
    project = group.project(names[1])
    build = project.build(names[2])

    build2_id = getid(build.status.baseline)
    build2 = first(project.builds(id__in=[build2_id]))

    return (group, project, build, build2)


# TODO: move to better location
# TODO: better name
def parse_changes(changes, environments, suites):
    regressions = {}
    fixes = {}
    unfiltered_regressions = changes["regressions"]
    unfiltered_fixes = changes["fixes"]
    for e in environments:
        for s in suites:
            key = (e.slug, s.slug)
            if (
                e.slug in unfiltered_regressions
                and s.slug in unfiltered_regressions[e.slug]
            ):
                regressions[key] = unfiltered_regressions[e.slug][s.slug]

            if e.slug in unfiltered_fixes and s.slug in unfiltered_fixes[e.slug]:
                fixes[key] = unfiltered_fixes[e.slug][s.slug]

    return regressions, fixes


class BuildNotFinished(Exception):
    """
    Raise when an operation cannot be completed due to an unfinished build.
    """


BuildKey = collections.namedtuple("BuildKey", "url group project build")


def build_results(build_key):
    SquadApi.configure(build_key.url)
    build = (
        Squad().group(build_key.group).project(build_key.project).build(build_key.build)
    )

    if not build.finished:
        raise BuildNotFinished(f"Build {build.version} has not yet finished")

    data = []
    for test in build.tests().values():
        environment = Environment().get(_id=getid(test.environment))
        suite = Suite().get(_id=getid(test.suite))

        data.append(
            {
                "group": build_key.group,
                "project": build_key.project,
                "build": build_key.build,
                "environment": environment.slug,
                "suite": suite.slug,
                "test": test.short_name,
                "status": test.status,
            }
        )

    return data
