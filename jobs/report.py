import jinja2


def build_template_env(templates):
    return jinja2.Environment(
        extensions=["jinja2.ext.loopcontrols"],
        loader=jinja2.FileSystemLoader(templates),
        trim_blocks=True,
        lstrip_blocks=True,
        undefined=jinja2.StrictUndefined,
    )
